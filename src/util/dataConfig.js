export const adminConfig = {
  administration: 'admin@admin.com'
};

export const menuList = [
  { title: '管理員管理系統', name: 'Managers', icon: 'fas fa-user-cog', disable: false },
  { title: '商品管理系統', name: 'Products', icon: 'fas fa-archive', disable: false },
  { title: '會員管理系統', name: 'Members', icon: 'fas fa-user-friends', disable: false },
  { title: '訂單管理系統', name: 'Orders', icon: 'fas fa-cart-arrow-down', disable: true },
  { title: '關於我們', name: 'AboutUs', icon: 'fas fa-id-card', disable: true },
  { title: '聯絡我們', name: 'ContactUs', icon: 'fas fa-paper-plane', disable: true },
  { title: '客製化後台管理', name: 'CustomSetting', icon: 'fas fa-golf-ball', disable: false },
];

export const membersColumns = [
  { title: 'ID', label: 'id', width: '' },
  { title: '姓名', label: 'name', width: '' },
  { title: '性別', label: 'gender', width: '' },
  { title: '手機', label: 'mobile', width: '' },
  { title: '生日', label: 'birthday', width: '' },
  { title: '狀態', label: 'status', width: '' },
];

export const managersColumns = [
  { title: '信箱', label: 'email', width: '' },
  { title: '姓名', label: 'name', width: '' },
  { title: '手機', label: 'mobile', width: '' },
  { title: '權限', label: 'auth', width: '' },
  { title: '狀態', label: 'status', width: '' },
];

export const breadcrumbTranslationData = {
  AboutUs: '關於我們',
  AddCustom: '新增項目',
  AddMember: '新增會員',
  AddManager: '新增管理員',
  AddProduct: '新增商品',
  AddProductCategory: '編輯類別',
  ContactUs: '聯絡我們',
  CustomSetting: '客製化後台管理',
  EditProductCategory: '編輯類別',
  EditCustom: '編輯項目',
  EditMember: '編輯會員',
  EditManager: '編輯管理員',
  EditProduct: '編輯商品',
  EditParent: '編輯資料',
  Managers: '管理員管理系統',
  Members: '會員管理系統',
  Orders: '訂單管理系統',
  ProductCategory: '類別管理',
  Products: '商品管理系統',
};
