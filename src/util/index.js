import Vue from 'vue';

export const setStore = (Vue, storeName, storeModule) => {
  if (!Vue.$store._modules.root._children[storeName]) {
    Vue.$store.registerModule(storeName, storeModule)
  }
}

export const orderIdToDate = (time) => {
  const date = new Date(parseInt(time));
  const year = date.getFullYear();
  let month = date.getMonth() + 1;
  month = month.toString().length === 1 ? `0${month}`: month;
  let day = date.getDate();
  day = day.toString().length === 1 ? `0${day}`: day;
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();

  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

export const setLang = (lang) => {
  Vue.config.lang = lang;
}
