import Vue from 'vue';
import Router from 'vue-router';
import store from '@/store';

// Client
const View = () => import(/* webpackChunkName: 'View' */ '@V/');
const Home = () => import(/* webpackChunkName: 'Home' */ '@V/Home');
const Shop = () => import(/* webpackChunkName: 'Shop' */ '@V/Shop');
const ShopCart = () => import(/* webpackChunkName: 'ShopCart' */ '@V/ShopCart');
const Register = () => import(/* webpackChunkName: 'Register' */ '@V/Account/Register');
const Products = () => import(/* webpackChunkName: 'Products' */ '@V/Products');
const Product = () => import(/* webpackChunkName: 'Product' */ '@V/Products/Product');
const Account = () => import(/* webpackChunkName: 'Account' */ '@V/Account/index');
const AccountLogin = () => import(/* webpackChunkName: 'AccountLogin' */ '@V/Account/AccountLogin');

// Admin
const Admin = () => import(/* webpackChunkName: 'Admin' */ '@A/');

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'View',
      component: View,
      beforeEnter: (to, from, next) => {
        if (to.path === '/') {
          next('/Shop')
        }
        next()
      },
      children: [
        {
          path: 'Shop',
          name: 'Shop',
          component: Shop,
        },
        {
          path: 'Register',
          name: 'Register',
          component: Register,
        },
        {
          path: 'Home',
          name: 'Home',
          component: Home,
        },
        {
          path: 'ShopCart',
          name: 'ShopCart',
          component: ShopCart,
        },
        {
          path: 'Products',
          name: 'Products',
          component: Products,
        },
        {
          path: 'Product',
          name: 'Product',
          component: Product,
        },
        {
          path: 'AccountLogin',
          name: 'AccountLogin',
          component: AccountLogin,
          beforeEnter: (to, from, next) => {
            const { uid } = store.state.Account.accountInfo;
            if (uid) {
              return next({ path: '/Account' });
            }
            next()
          }
        },
        {
          path: 'Account',
          name: 'Account',
          component: Account,
          beforeEnter: (to, from, next) => {
            console.log(store.state.Account.accountInfo);
            const { uid } = store.state.Account.accountInfo;
            if (!uid) {
              return next({ path: '/AccountLogin' });
            }
            next()
          },
        },
      ]
    },
    {
      path: '/Admin',
      name: 'Admin',
      component: Admin
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    // return 期望滚动到哪个的位置
    return { x: 0, y: 0 };
  }
});
