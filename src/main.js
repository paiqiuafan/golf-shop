// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import ElementUI from 'element-ui';
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import locales from '@/assets/i18n';
import App from './App';
import router from './router';
import store from './store';
import '@/firebase';
import './css/reset.scss';
import './css/fontawesome-free-5.5.0-web/css/all.css';
import 'element-ui/lib/theme-chalk/index.css';

import './assets/js/jquery-2.1.3.min.js';
import './assets/css/fonts.css';
import './assets/css/style.css';
import './assets/css/include.css';
import './maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css';
// import './assets/js/script.js';
import 'fabric'

Vue.use(ElementUI);
Vue.use(VueI18n);
Vue.config.productionTip = false;
Vue.config.lang = 'tw';

Object.keys(locales).forEach((lang) => {
  Vue.locale(lang, locales[lang]);
});

/* eslint-disable no-new */
export const $vue = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});
