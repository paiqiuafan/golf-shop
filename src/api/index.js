console.log(window.location.origin);
const mode = {
  'http://localhost:8081': 'local',
  'https://golf-shop-35cb5.firebaseapp.com': 'dev',
  'https://custom.omnixgolf.com': 'dev'
};

const firebaseDomain = {
  local: 'http://localhost:5001/golf-shop-35cb5/us-central1/',
  dev: 'https://us-central1-golf-shop-35cb5.cloudfunctions.net/'
};

const path = {
  deleteUser: 'deleteUser'
};

const makeUrl = (key) => {
  return `${firebaseDomain[mode[window.location.origin]]}${path[key]}`;
}

export const Fetch = (path, body) => {
  const url = makeUrl(path);
  return fetch(url, {
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body),
    mode: 'no-cors',
    method: 'POST'
  });
}
