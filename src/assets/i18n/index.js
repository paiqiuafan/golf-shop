import tw from './zh_TW.json';
import en from './en_US.json';

export default {
  tw,
  en,
};
