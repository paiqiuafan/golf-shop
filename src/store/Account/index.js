import * as types from '../mutations_type.js';
import app from '@/firebase';
import { $vue } from '@/main';

const dbName = 'Users';
const AUTH = app().auth();
const DB = app().database();
const accountInfo = JSON.parse(localStorage.getItem('accountInfo'));

const setLocalStorage = (data, key = 'accountInfo') => {
  localStorage.setItem(key, JSON.stringify(data));
}

const removeAccountInfo = (key = 'accountInfo') => {
  localStorage.removeItem(key);
}

const state = {
  accountInfo: accountInfo || {},
}

const actions = {
  accountLogin({ commit, dispatch }, payload) {
    const { email, password } = payload;
    const persistence = AUTH.setPersistence(AUTH.app.firebase_.auth.Auth.Persistence.SESSION);
    const signInWithEmailAndPassword = AUTH.signInWithEmailAndPassword(email, password).then(
      res => dispatch('fetchAccountInfo', { uid: res.user.uid }));
    Promise.all([persistence, signInWithEmailAndPassword]).then((res) => {
      $vue.$message({
        showClose: true,
        message: `登入成功`,
        type: 'success'
      });
      $vue.$router.push({ path: 'Account' });
    }).catch((err) => {
      $vue.$message({
        showClose: true,
        message: `登入失敗<${err.message}>`,
        type: 'error'
      });
    });;
  },
  accountLogout({ commit }) {
    removeAccountInfo();
    commit(types.SET_ACCOUNT_INFO, {});
    $vue.$message({
      showClose: true,
      message: `登出成功`,
      type: 'success'
    });
  },
  fetchAccountInfo({ commit }, payload) {
    const { uid } = payload;
    return new Promise((resolve, reject) => {
      DB.ref(`${dbName}/members/${uid}`).on('value', (snapshot) => {
        console.log(snapshot.val());
        commit(types.SET_ACCOUNT_INFO, snapshot.val());
        setLocalStorage(snapshot.val());
        resolve(snapshot.val())
      })
    })
  }
}

const mutations = {
  [types.SET_ACCOUNT_INFO](state, payload) {
    state.accountInfo = payload;
  }
}

const getters = {
  accountInfo: state => state.accountInfo
}

export default {
  state,
  mutations,
  actions,
  getters,
}
