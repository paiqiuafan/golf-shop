import * as types from '../mutations_type.js';
import Vue from 'vue';
import { $vue } from '@/main';
import { Fetch } from '@/api';

// database 資料節點名稱
const dbName = 'Users';
const DB = Vue.prototype.$firebase.database();
const AUTH = Vue.prototype.$firebase.auth();

const state = {
  memberList: {},
  managerList: {}
}

const actions = {
  registerUser({ commit }, payload) {
    const {
      name,
      email,
      mobile = null,
      country = null,
      area = null,
      gender = null,
      birthday = null,
      height = null,
      weight = null,
      auth,
      status = true,
      password = null,
      callback } = payload;
    const createUser = (uid) => {
      const id = new Date().getTime();
      const authPath = auth === 'member' ? 'members' : 'managers';
      return DB.ref(`${dbName}/${authPath}/${uid}`).set({
        id,
        uid,
        name,
        email,
        mobile,
        country,
        area,
        gender,
        birthday,
        height,
        weight,
        auth,
        status
      });
    }
    const authPass = (password || mobile).toString();
    AUTH.createUserWithEmailAndPassword(email, authPass).then((res) => {
      createUser(res.user.uid).then((res) => {
        $vue.$message({
          message: '恭喜你，这是一条成功消息',
          type: 'success'
        });
        callback();
        $vue.$store.dispatch('closeSubMain');
        $vue.$router.push({ path: 'AccountLogin' });
      })
    }).catch((error) => {
      $vue.$message({
        showClose: true,
        message: error.message,
        type: 'error'
      });
    })
  },
  fetchMemberList({ commit }) {
    DB.ref(`${dbName}/members`).on('value', (snapshot) => {
      commit(types.SET_MEMBER_LIST, snapshot.val())
    })
  },
  fetchManagerList({ commit }) {
    DB.ref(`${dbName}/managers`).on('value', (snapshot) => {
      commit(types.SET_MANAGER_LIST, snapshot.val())
    })
  },
  updateMember({ commit, getters }, payload) {
    const {
      id,
      uid,
      name,
      email,
      mobile,
      address = null,
      gender = null,
      birthday = null,
      height = null,
      weight = null,
      auth,
      status = true } = payload;
    console.log(payload);
    const authPath = auth === 'member' ? 'members' : 'managers';
    let newData = auth === 'member' ? getters.getMemberList[uid] : getters.getManagerList[uid];
    Object.assign(newData, {
      name,
      email,
      mobile,
      address,
      gender,
      birthday,
      height,
      weight,
      auth,
      status
    });
    DB.ref(`${dbName}/${authPath}/${uid}`).set(newData).then((res) => {
      $vue.$message({
        message: '恭喜你，这是一条成功消息',
        type: 'success'
      });
      $vue.$store.dispatch('closeSubMain');
    })
  },
  deleteUser({ commit, getters, dispatch }, payload) {
    const { auth } = payload;
    dispatch('setDialogData', {
      show: true,
      confirm: () => {
        Fetch('deleteUser', payload).then((res) => {
          $vue.$message({
            message: '恭喜你，这是一条成功消息',
            type: 'success'
          });
          if (auth === 'member') {
            commit(types.DELETE_MEMBER, payload);
          } else {
            commit(types.DELETE_MANAGER, payload);
          }
          dispatch('setDialogData', { show: false });
        }).catch((err) => {
          $vue.$message({
            showClose: true,
            message: err.error,
            type: 'error'
          });
        });
      },
      content: '確定刪除？'
    });
  }
}

const mutations = {
  [types.SET_MEMBER_LIST](state, payload) {
    state.memberList = { ...state.memberList, ...payload };
  },
  [types.SET_MANAGER_LIST](state, payload) {
    state.managerList = { ...state.managerList, ...payload };
  },
  [types.DELETE_MANAGER](state, payload) {
    Vue.delete(state.managerList, payload.uid);
  },
  [types.DELETE_MEMBER](state, payload) {
    Vue.delete(state.memberList, payload.uid);
  }
}

const getters = {
  getMemberList: state => state.memberList,
  getManagerList: state => state.managerList
}

export default {
  state,
  mutations,
  actions,
  getters,
}
