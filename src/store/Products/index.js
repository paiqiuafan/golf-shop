import * as types from '../mutations_type.js'
import { updateImage, createImage, deleteImage, fetchImageUrl } from '../firebaseTool.js'
import Vue from 'vue'
import { $vue } from '@/main'

// database 資料節點名稱
const dbName = 'Products';
const categoryDBName = 'ProductCategory';
const DB = Vue.prototype.$firebase.database()

const state = {
  productCategoryList: {},
  productList: {},
  productSelectId: 0
}

const actions = {
  async createProduct({ commit, getters, dispatch }, payload) {
    const {
      name = null,
      categoryId = null,
      material = null,
      color = null,
      productNo = null,
      weight = null,
      caliber = null,
      file = [] } = payload;
    const id = new Date().getTime();
    let files = await dispatch('uploadImages', { id, file });
    const categoryName = getters.productCategoryList[categoryId].name;
    DB.ref(`${dbName}/${id}`).set({
      id,
      name,
      categoryId,
      categoryName,
      material,
      color,
      productNo,
      weight,
      caliber,
      files
    }).then((res) => {
      $vue.$message({
        message: '恭喜你，这是一条成功消息',
        type: 'success'
      });
    });
  },
  createProductCategory({ commit }, payload) {
    const { name, status } = payload;
    const id = new Date().getTime();
    DB.ref(`${categoryDBName}/${id}`).set({
      id,
      name,
      status
    }).then((res) => {
      $vue.$message({
        message: '恭喜你，这是一条成功消息',
        type: 'success'
      });
      $vue.$store.dispatch('closeSubMain');
    });
  },
  fetchProductCategory({ commit }) {
    DB.ref(`${categoryDBName}`).on('value', (snapshot) => {
      commit(types.SET_PRODUCT_CATEGORY_LIST, snapshot.val());
    });
  },
  fetchProductList({ commit }) {
    DB.ref(`${dbName}`).on('value', (snapshot) => {
      commit(types.SET_PRODUCT_LIST, snapshot.val());
    });
  },
  updateProductCategory({ commit }, payload) {
    const { id, name, status } = payload;
    DB.ref(`${categoryDBName}/${id}`).set({
      id,
      name,
      status
    }).then((res) => {
      $vue.$message({
        message: '恭喜你，这是一条成功消息',
        type: 'success'
      });
      $vue.$store.dispatch('closeSubMain');
    })
  },
  async updateProduct({ commit, dispatch, getters }, payload) {
    const {
      id,
      name = null,
      categoryId = null,
      material = null,
      color = null,
      productNo = null,
      weight = null,
      caliber = null,
      file = [],
      files = [],
      oldFile = [] } = payload;
    let uploadFiles = [];
    const fileName = file.map(item => item.name);
    const oldFileName = oldFile.map(item => item.name);
    const haveToRemoveImage = oldFile.filter((item, index) => {
      if (fileName.indexOf(item.name) === -1) {
        files.splice(index, 1);
        return true;
      }
      return false;
      return fileme.indexOf(item.name) === -1;
    });
    const haveToCreateImage = file.filter(item => oldFileName.indexOf(item.name) === -1);
    console.log('haveToRemoveImage', haveToRemoveImage);
    if (haveToRemoveImage.length) {
      await dispatch('removeImages', { id, files: haveToRemoveImage });
    }
    if(haveToCreateImage.length) {
      uploadFiles = await dispatch('uploadImages', { id, file: haveToCreateImage });
    }
    const categoryName = getters.productCategoryList[categoryId].name;
    DB.ref(`${dbName}/${id}`).set({
      id,
      name,
      categoryId,
      categoryName,
      material,
      color,
      productNo,
      weight,
      caliber,
      files: files.concat(uploadFiles)
    }).then((res) => {
      $vue.$message({
        message: '恭喜你，这是一条成功消息',
        type: 'success'
      });
    });
  },
  deleteProductCategory({ commit, getters, dispatch }, payload) {
    dispatch('setDialogData', {
      show: true,
      confirm: () => {
        DB.ref(`${categoryDBName}/${payload.id}`).remove();
        dispatch('setDialogData', { show: false });
      },
      content: '確定刪除？'
    })
  },
  deleteProduct({ commit, dispatch }, payload) {
    const { id, files } = payload;
    dispatch('setDialogData', {
      show: true,
      confirm: async () => {
        const deleteStorage = dispatch('deleteImage', { path: `CustomSetting/${payload.imageUrl}` })
        await dispatch('removeImages', payload);
        DB.ref(`${dbName}/${id}`).remove();
        dispatch('setDialogData', { show: false })
      },
      content: '確定刪除？'
    })
  },
  setProductSelectId({ commit }, payload) {
    commit(types.SET_PRODUCT_SELECT_ID, payload)
  },
  uploadImages({ dispatch }, payload) {
    const { file, id } = payload;
    const files = [];
    return new Promise((resolve, reject) => {
      file.forEach((item, index) => {
        const defaultFile = item.raw;
        const fileName = item.name;
        const imageUrl = `${id}_${fileName}`;
        dispatch('createImage', {
          id,
          path: `Products`,
          file: defaultFile
        }).then((res) => {
          console.log('res', res);
          files.push({
            fileName,
            imageUrl
          });
          if (files.length === file.length) {
            console.log('files', files);
            return resolve(files)
          }
        });
      });
    });
  },
  removeImages({ commit, dispatch }, payload) {
    const { id, files } = payload;
    const success = [];
    console.log(files);
    return new Promise((resolve, reject) => {
      if (!files.length) {
        return resolve(success);
      }
      files.forEach((item) => {
        const path = item.imageUrl ? imageUrl : `${id}_${item.name}`;
        dispatch('deleteImage', {
          path: `Products/${path}`
        }).then((res) => {
          success.push(res);
          if (files.length === success.length) {
            console.log('success', success);
            return resolve(success)
          }
        });
      });
    });
  },
  updateImage,
  createImage,
  deleteImage,
  fetchImageUrl
}

const mutations = {
  [types.SET_PRODUCT_CATEGORY_LIST](state, payload) {
    state.productCategoryList = payload;
  },
  [types.SET_PRODUCT_LIST](state, payload) {
    state.productList = payload;
  },
  [types.SET_PRODUCT_SELECT_ID](stste, payload) {
    state.productSelectId = payload;
  }
}

const getters = {
  productCategoryList: state => state.productCategoryList,
  productList: state => state.productList,
  productSelectId: state => state.productSelectId
}

export default {
  state,
  mutations,
  actions,
  getters,
}
