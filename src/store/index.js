import Vue from 'vue'
import Vuex from 'vuex';
Vue.use(Vuex);

import App from './App';
import Account from './Account'

export default new Vuex.Store({
  modules: {
    App,
    Account
  }
})
