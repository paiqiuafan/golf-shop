import * as types from '../mutations_type.js'
import { updateImage, createImage, deleteImage, fetchImageUrl } from '../firebaseTool.js'
import Vue from 'vue'
import { $vue } from '@/main'

// database 資料節點名稱
const dbName = 'CustomSetting';
const infoDBName = 'CustomInfo';
const DB = Vue.prototype.$firebase.database()

const state = {
  category: 'Primary',
  customSettingList: {},
  customSelectId: 0,
  customInfoList: {}
}

const actions = {
  setCategory({ commit }, payload) {
    commit(types.SET_CATEGORY, payload)
  },
  async createCustomSetting({ commit, getters, dispatch }, payload) {
    const {
      name,
      color,
      file,
      productNo,
      price } = payload
    const id = new Date().getTime()
    const imageUrl = `${id}_${file[0].name}`
    const defaultFile = file[0].raw
    const createDB = DB.ref(`${dbName}/${getters.customCategory}/${id}`).set({
      id,
      name,
      color,
      imageUrl,
      productNo,
      price
    });
    const createStorage = dispatch('createImage', {
      id,
      path: `CustomSetting`,
      file: defaultFile
    })
    await Promise.all([createDB, createStorage]).then((value) => {
      console.log('value', value);
      $vue.$message({
        message: '恭喜你，这是一条成功消息',
        type: 'success'
      });
    }).catch((error) => {
      $vue.$message({
        showClose: true,
        message: '错了哦，这是一条错误消息',
        type: 'error'
      });
    })
  },
  fetchCustomSettingList({ commit }) {
    DB.ref(`${dbName}`).on('value', (snapshot) => {
      commit(types.SET_CUSTOM_SETTING_LIST, snapshot.val())
    })
  },
  deleteCustomSetting({ commit, getters, dispatch }, payload) {
    dispatch('setDialogData', {
      show: true,
      confirm: () => {
        const deleteStorage = dispatch('deleteImage', { path: `CustomSetting/${payload.imageUrl}` })
        const deleteDB = DB.ref(`${dbName}/${getters.customCategory}/${payload.id}`).remove()
        Promise.all([deleteStorage, deleteDB]).then((value) => {
          console.log(value);
        })
        dispatch('setDialogData', { show: false })
      },
      content: '確定刪除？'
    })
  },
  setCustomSelectId({ commit }, payload) {
    commit(types.SET_CUSTOM_SELECT_ID, payload)
  },
  async updateCustomSetting({ commit, getters, dispatch }, payload) {
    const {
      id,
      name,
      color,
      price,
      file,
      oldFile,
      productNo } = payload
    let imageUrl = `${id}_${file[0].name}`;
    let isNewImage = true;
    if (file[0].name === oldFile[0].name) {
      imageUrl = `${file[0].name}`;
      isNewImage = false;
    }
    const updateDB = DB.ref(`${dbName}/${getters.customCategory}/${id}`).update({
      id,
      name,
      color,
      price,
      imageUrl,
      productNo
    })
    const updateStorage = isNewImage && dispatch('updateImage', {
      id,
      path: `CustomSetting`,
      file: file[0].raw,
      oldFile: oldFile[0],
      imageUrl
    })
    await Promise.all([updateDB, updateStorage]).then((value) => {
      $vue.$message({
        message: '更新成功！',
        type: 'success'
      });
    }).catch((error) => {
      $vue.$message({
        showClose: true,
        message: '更新失敗！',
        type: 'error'
      });
    })
  },
  updateCustomInfo({ commit, getters }, payload) {
    const { name, enable } = payload;
    DB.ref(`${infoDBName}/${getters.customCategory}`).set({
      name,
      enable
    }).then((res) => {
      $vue.$message({
        message: '恭喜你，这是一条成功消息',
        type: 'success'
      });
      $vue.$store.dispatch('closeSubMain');
    })
  },
  fetchCustomInfo({ commit }) {
    DB.ref(`${infoDBName}`).on('value', (snapshot) => {
      commit(types.SET_CUSTOM_INFO_LIST, snapshot.val())
    })
  },
  updateImage,
  createImage,
  deleteImage,
  fetchImageUrl
}

const mutations = {
  [types.SET_CATEGORY](state, payload) {
    state.category = payload
  },
  [types.SET_CUSTOM_SETTING_LIST](state, payload) {
    state.customSettingList = payload || {}
  },
  [types.SET_CUSTOM_SELECT_ID](state, payload) {
    state.customSelectId = payload
  },
  [types.SET_CUSTOM_INFO_LIST](state, payload) {
    state.customInfoList = payload;
  }
}

const getters = {
  customCategory: state => state.category,
  customSettingList: state => state.customSettingList,
  customSelectId: state => state.customSelectId,
  customInfoList: state => state.customInfoList
}

export default {
  state,
  mutations,
  actions,
  getters,
}
