import * as types from '../mutations_type.js'

const breadcrumb = JSON.parse(localStorage.getItem('breadcrumb'))

const state = {
  mainName: (breadcrumb && breadcrumb[breadcrumb.length - 1]) || 'Managers',
  breadcrumb: breadcrumb || [],
  dialogData: {
    show: false,
    content: '',
    confirm: () => {}
  },
  subMainData: {
    show: false,
    name: '',
    id: ''
  }
}

const actions = {
  setMainName({ commit, dispatch }, payload = 'Managers') {
    commit(types.SET_MAIN_NAME, payload);
    dispatch('setBreadcrumb', payload)
  },
  pushBreadcrumb({ commit }, payload) {
    commit(types.SET_BREADCRUMB, { type: 1, data: payload });
    commit(types.SET_MAIN_NAME, payload)
  },
  setBreadcrumb({ commit }, payload) {
    commit(types.SET_BREADCRUMB, { type: 2, data: payload });
  },
  popBreadcrumb({ commit }) {
    commit(types.SET_BREADCRUMB, { type: 3 });
  },
  setDialogData({ commit }, payload) {
    commit(types.SET_DIALOG_DATA, payload)
  },
  setSubMainData({ commit }, payload) {
    commit(types.SET_SUB_MAIN_DATA, payload)
  },
  closeSubMain({ commit }) {
    commit(types.SET_SUB_MAIN_DATA, { show: false, name: '' })
  }
}

const mutations = {
  [types.SET_MAIN_NAME](state, payload) {
    state.mainName = payload;
  },
  [types.SET_BREADCRUMB](state, payload) {
    switch(payload.type) {
      // 增加
      case 1:
        state.breadcrumb.push(payload.data);
        break;
      // 菜單使用
      case 2:
        state.breadcrumb = []
        state.breadcrumb[0] = payload.data
        break;
      // 刪除最後一個
      case 3:
        state.breadcrumb.pop();
        state.mainName = state.breadcrumb[state.breadcrumb.length - 1]
        break;
    }
    const breadcrumbTemp = JSON.parse(JSON.stringify(state.breadcrumb))
    while (breadcrumbTemp.length > 1) {
      breadcrumbTemp.pop()
    }
    localStorage.setItem('breadcrumb', JSON.stringify(breadcrumbTemp));
  },
  [types.SET_DIALOG_DATA](state, payload) {
    state.dialogData = { ...state.dialogData, ...payload }
  },
  [types.SET_SUB_MAIN_DATA](state, payload) {
    state.subMainData = { ...state.subMainData, ...payload }
  }
}

const getters = {
  mainName: state => state.mainName,
  breadcrumb: state => state.breadcrumb,
  dialogData: state => state.dialogData,
  subMainData: state => state.subMainData
}

export default {
  state,
  mutations,
  actions,
  getters,
}
