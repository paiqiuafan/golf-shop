import Vue from 'vue'
const STORAGE = Vue.prototype.$firebase.storage()

// 更新圖片
export const updateImage = ({ commit }, payload) => {
  const { id, path, file, oldFile } = payload
  return STORAGE.ref().child(`${path}/${file.name}`).put(file, {
    contentType: file.type
  }).then(() => STORAGE.ref().child(`${path}/${oldFile.name}`).delete())
}

// 新增圖片
export const createImage = ({ commit }, payload) => {
  const { id, path, file } = payload
  return STORAGE.ref().child(`${path}/${id}_${file.name}`).put(file, {
    contentType: file.type
  })
}

// 刪除圖片
export const deleteImage = ({ commit }, payload) => {
  const { path } = payload
  return STORAGE.ref().child(path).delete()
}

// 取圖片
export const fetchImageUrl = ({ commit }, payload) => {
  const { path } = payload
  return STORAGE.ref().child(path).getDownloadURL()
}
