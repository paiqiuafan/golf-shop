import * as types from '../mutations_type.js';
import { updateImage, createImage, deleteImage, fetchImageUrl } from '../firebaseTool.js'
import Vue from 'vue';
import { $vue } from '@/main';

// database 資料節點名稱
const dbName = 'Orders';
const DB = Vue.prototype.$firebase.database();

const cartListKey = 'cartList';
const cartListCustomKey = 'cartListCustom';
const cartList = JSON.parse(localStorage.getItem(cartListKey));
const cartListCustom = JSON.parse(localStorage.getItem(cartListCustomKey));

const setLocalStorage = (data, key = cartListKey) => {
  localStorage.setItem(key, JSON.stringify(data));
}

const removeCartList = (key = cartListKey) => {
  localStorage.removeItem(key);
}

const state = {
  orderList: {},
  cartList: cartList || {},
  cartListCustom: cartListCustom || {},
  orderSelectId: 0
}

const actions = {
  fetchOrderList({ commit }) {
    DB.ref(`${dbName}`).orderByKey().on('value', (snapshot) => {
      commit(types.SET_ORDER_LIST, snapshot.val());
    });
  },
  createOrder({ commit, getters, dispatch }, payload) {
    const orderId = new Date().getTime();
    const progress = 'unprocessed';
    const orderData = {
      orderId,
      ...getters.accountInfo,
      cartList: payload.cartList ? payload.cartList : null,
      cartListCustom: payload.cartListCustom ? payload.cartListCustom : null,
      progress
    };
    DB.ref(`${dbName}/${orderId}`).set(orderData).then((res) => {
      dispatch('setDialogData', {
        show: true,
        confirm: () => {
          dispatch('setDialogData', { show: false })
        },
        content: '感謝您的訂購，將會有專員與您聯繫。'
      });
      dispatch('cleanCartList');
    })
  },
  addCartList({ commit }, payload) {
    commit(types.ADD_CART, payload);
  },
  addCartListCustom({ commit }, payload) {
    payload.id = new Date().getTime();
    commit(types.ADD_CART_CUSTOM, payload);
  },
  removeCartList({ commit, dispatch }, payload) {
    dispatch('setDialogData', {
      show: true,
      confirm: () => {
        commit(types.REMOVE_CART, payload);
        dispatch('setDialogData', { show: false })
      },
      content: '確定刪除？'
    });
  },
  removeCartListCustom({ commit, dispatch }, payload) {
    dispatch('setDialogData', {
      show: true,
      confirm: () => {
        commit(types.REMOVE_CART_CUSTOM, payload);
        dispatch('setDialogData', { show: false })
      },
      content: '確定刪除？'
    });
  },
  cleanCartList({ commit }) {
    commit(types.CLEAN_CART_LIST);
  },
  changeCartAmount({ commit }, payload) {
    commit(types.CHANGE_CART_AMOUNT, payload);
  },
  changeCartAmountCustom({ commit }, payload) {
    commit(types.CHANGE_CART_AMOUNT_CUSTOM, payload);
  },
  setOrderSelectId({ commit }, payload) {
    commit(types.SET_ORDER_SELECT_ID, payload)
  },
  fetchImageUrl
}

const mutations = {
  [types.SET_ORDER_LIST](state, payload) {
    state.orderList = { ...state.orderList, ...payload };
  },
  [types.ADD_CART](state, payload) {
    const isExist = !!state.cartList[payload.id];
    if (isExist) {
      payload.amount = state.cartList[payload.id].amount + payload.amount;
    }
    state.cartList = { ...state.cartList, ...{ [payload.id]: payload } };
    setLocalStorage(state.cartList);
    $vue.$message({
      message: '已新增至購物車',
      type: 'success'
    });
  },
  [types.ADD_CART_CUSTOM](state, payload) {
    state.cartListCustom = { ...state.cartListCustom, ...{ [payload.id]: payload } };
    setLocalStorage(state.cartListCustom, cartListCustomKey);
    $vue.$message({
      message: '已新增至購物車',
      type: 'success'
    });
  },
  [types.REMOVE_CART](state, payload) {
    const id = payload.id;
    const tempData = JSON.parse(JSON.stringify(state.cartList));
    state.cartList = {};
    Object.keys(tempData).forEach((key) => {
      if (parseInt(key) !== id) {
        state.cartList = { ...state.cartList, ...{ [key]: tempData[key] } };
      }
    });
    setLocalStorage(state.cartList);
  },
  [types.REMOVE_CART_CUSTOM](state, payload) {
    const id = payload.id;
    const tempData = JSON.parse(JSON.stringify(state.cartListCustom));
    state.cartListCustom = {};
    Object.keys(tempData).forEach((key) => {
      if (parseInt(key) !== id) {
        state.cartListCustom = { ...state.cartListCustom, ...{ [key]: tempData[key] } };
      }
    });
    setLocalStorage(state.cartListCustom, cartListCustomKey);
  },
  [types.CLEAN_CART_LIST](state) {
    removeCartList();
    state.cartList = {};

    removeCartList(cartListCustomKey);
    state.cartListCustom = {};
  },
  [types.CHANGE_CART_AMOUNT](state, payload) {
    const { id, amount } = payload;
    state.cartList[id].amount = amount;
    setLocalStorage(state.cartList);
  },
  [types.CHANGE_CART_AMOUNT_CUSTOM](state, payload) {
    const { id, amount } = payload;
    state.cartListCustom[id].amount = amount;
    setLocalStorage(state.cartListCustom, cartListCustomKey);
  },
  [types.SET_ORDER_SELECT_ID](state, payload) {
    state.orderSelectId = payload;
  }
}

const getters = {
  getOrderList: state => state.orderList,
  cartList: state => state.cartList,
  cartListCustom: state => state.cartListCustom,
  orderSelectId: state => state.orderSelectId
}

export default {
  state,
  mutations,
  actions,
  getters,
}
