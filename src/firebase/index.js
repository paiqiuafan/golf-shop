import Firebase from 'firebase/app';
import Vue from 'vue';
import config from './config.json';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/auth';

let firebaseInit = null;

export default function () {
  if (Firebase.apps.length === 0) {
    firebaseInit = Firebase.initializeApp(config);
    Vue.prototype.$firebase = Firebase
  }
  return firebaseInit;
}
