const functions = require('firebase-functions');
const admin = require('firebase-admin');
const serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://golf-shop-35cb5.firebaseio.com"
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.deleteUser = functions.https.onRequest((req, res) => {
  const { uid, auth } = JSON.parse(req.body);
  const authPath = auth === 'member' ? 'members' : 'managers';
  if (!uid) {
    return res.status(400).send(errorParamater());
  }

  const authPromise = admin.auth().deleteUser(uid);
  const dbPromise = admin.database().ref(`Users/${authPath}/${uid}`).remove();
  Promise.all([authPromise, dbPromise]).then((value) => {
    return res.status(200).send(seccessMessage());
  }).catch((error) => {
    return res.status(400).send(errorMessage(error.message));
  });
});

function seccessMessage() {
  return {
    status: 'Success'
  };
}

function errorMessage(message = undefined) {
  return {
    error: message
  };
}

function errorParamater() {
  return {
    error: 'parameter error'
  }
}
